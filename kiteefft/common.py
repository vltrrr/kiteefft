import sys
from pathlib import Path


_VERSION_PATH = (Path(__file__).parents[1] / 'VERSION')
with _VERSION_PATH.open() as handle:
    VERSION = handle.read()

KITEEFFT_VERSION = f'KiteeFFT {VERSION}'


def fatal_err(message: str):
    print(f'ERROR: {message}')
    sys.exit(1)
