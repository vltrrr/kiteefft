import sys

from PySide2 import QtWidgets

from kiteefft.gui import KiteeGUI
from kiteefft.common import *


def main():
    print("kiteefft_gui ({:s})".format(KITEEFFT_VERSION))

    app = QtWidgets.QApplication(sys.argv)
    kitee_gui = KiteeGUI()
    kitee_gui.show()

    sys.exit(app.exec_())
