from typing import List, Set, Dict, Tuple, Optional
from pathlib import Path

import argparse

from .common import *
from . import wav_spectrum, plot


# _____________________________________________________________________________
# args

def _parse_args():
    parser = argparse.ArgumentParser(description='KiteeFFT CLI interface')

    parser.add_argument(
        'wav_files',
        help='input wav file(s)',
        nargs='+'
    )
    parser.add_argument(
        '-w',
        dest='window',
        help='Window function',
        choices=['gauss', 'blackman']
    )
    parser.add_argument(
        '--xrange',
        dest='x_range',
        help='x axis range start:end, eg. 10:200',
    )
    parser.add_argument(
        '--yrange',
        dest='y_range',
        help='y axis range start:end, eg. 0:0.5',
    )
    parser.add_argument(
        '--xtic',
        dest='x_tic',
        help='x axis tics, <major>:<minor>, eg. 25:5',
    )
    parser.add_argument(
        '--logx',
        dest='enable_log_x',
        help='Use log x axis',
        default=False,
        action='store_true',
    )
    parser.add_argument(
        '-f',
        dest='out_format',
        help='Output format',
        choices=['png', 'svg'],
        default='png'
    )
    parser.add_argument(
        '-o',
        dest='out_file',
        help='Output file path',
    )

    return parser.parse_args()


def _parse_range(range: str, ensure_positive=False):
    range_split = range.split(':')
    if len(range_split) != 2:
        fatal_err(f'invalid range {range}')

    try:
        a = float(range_split[0])
        b = float(range_split[1])
    except ValueError:
        fatal_err(f'invalid range {range}')

    if ensure_positive:
        if a > b:
            a, b = b, a

    return a, b


# _____________________________________________________________________________
# helpers

def _process_spec(wspec: wav_spectrum.WavSpectrum, window: Optional[str] = None):
    wspec.preproc_stereo()

    if window:
        wspec.preproc_window(window)

    wspec.preproc_zerofill()
    wspec.fft()
    wspec.ft_normalize_n_of_samples()


# _____________________________________________________________________________
# main

def _init_plot(args):
    splot = plot.SpecPlot()
    splot.setup()

    if args.x_range:
        x_min, x_max = _parse_range(args.x_range, ensure_positive=True)
        splot.setup_x_ax(x_min=x_min, x_max=x_max, log_scale=args.enable_log_x)
    else:
        splot.setup_x_ax(log_scale=args.enable_log_x)

    if args.x_tic:
        major, minor = _parse_range(args.x_tic)
        splot.setup_ax_tic(major, minor)

    if args.y_range:
        y_min, y_max = _parse_range(args.y_range, ensure_positive=True)
        splot.setup_y_ax(y_min=y_min, y_max=y_max)

    return splot


def main():
    print(KITEEFFT_VERSION)
    print('')

    args = _parse_args()
    splot = _init_plot(args)

    print(f'processing:')
    for i, in_file in enumerate(args.wav_files):
        print(f'  - {in_file}')
        in_path = Path(in_file)
        if not in_path.is_file():
            fatal_err(f'File not found {in_path}')

        wspec = wav_spectrum.WavSpectrum()
        try:
            wspec.read(in_file)
        except ValueError as ve:
            fatal_err(f'error reading .wav file: {ve}')
        _process_spec(wspec, window=args.window)

        splot.plot_line(wspec.freq_x, wspec.freq_y, label=in_path.stem, color_index=i)

    splot.setup_legend()
    print('')

    out_file = args.out_file or Path(args.wav_files[0]).stem
    if not out_file.endswith(f'.{args.out_format}'):
        out_file = f'{out_file}.{args.out_format}'

    print(f'writing {out_file}')
    splot.save(out_file, format=args.out_format)
