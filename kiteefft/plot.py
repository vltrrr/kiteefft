from typing import List, Set, Dict, Tuple, Optional, Union

from pathlib import Path

import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib.figure import Figure


class SpecPlot:
    LINE_COLORS = [
        'crimson',
        'royalblue',
        'orchid',
        'black',
    ]

    def __init__(self):
        self.fig = Figure()
        self.axes = self.fig.add_subplot(111)
        self.fig.patch.set_facecolor('white')

    def setup(self, width=200, height=120):
        self.fig.set_size_inches((width/25.4, height/25.4), forward=True)
        self.fig.set_dpi(85)
        self.fig.set_tight_layout(True)

        self.axes.grid(linestyle = '--', linewidth = 0.5)
        self.axes.set_xlabel('frequency (Hz)')
        self.axes.set_ylabel('intensity')

    def setup_legend(self, loc='upper right'):
        self.axes.legend(loc=loc)

    def setup_ax_tic(self, major_tic, minor_tic, axis=0):
        major_locator = plt.MultipleLocator(major_tic)
        minor_locator = plt.MultipleLocator(minor_tic)

        if axis == 0:
            self.axes.xaxis.set_major_locator(major_locator)
            self.axes.xaxis.set_minor_locator(minor_locator)
        else:
            self.axes.yaxis.set_major_locator(major_locator)
            self.axes.yaxis.set_minor_locator(minor_locator)

    def setup_x_ax(self, x_min=10, x_max=20_000, log_scale=True):
        if log_scale:
            self.axes.set_xscale('log')

            # audio freq tics
            self.axes.set_xticks([20, 50, 100, 200, 500, 1000, 2000, 4000, 8000, 16000])
            self.axes.get_xaxis().set_major_formatter(mpl.ticker.ScalarFormatter())

        self.axes.set_xlim(x_min, x_max)

    def setup_y_ax(self, y_min: float, y_max: float):
        self.axes.set_ylim(y_min, y_max)

    def plot_line(self, x_arr, y_arr, linestyle='-', lw=1.5, color_index=0, alpha=0.8, label=''):
        line, = self.axes.plot(x_arr, y_arr, alpha=alpha, label=label)

        color_index = color_index % len(self.LINE_COLORS)
        line.set_color(self.LINE_COLORS[color_index])
        line.set_linewidth(lw)
        line.set_linestyle(linestyle)

    def save(self, path: str, format='png'):
        assert format in ['png', 'svg']
        self.fig.savefig(path, format=format)
