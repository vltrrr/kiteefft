from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from PySide2 import QtWidgets, QtCore

from kiteefft import wav_spectrum
from kiteefft.common import *
from kiteefft.gui import canvas, proc_thread


# _____________________________________________________________________________
# GUI widgets/classes

class KiteeMainWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

        self.wspec = wav_spectrum.WavSpectrum()
        self.init_main_text()

    def initUI(self):
        self.init_widgets()

        self.wav_select.clicked.connect(self.show_select_wav)
        self.fid_button.clicked.connect(self.load_wav)
        self.gausswin_button.clicked.connect(self.apodize_wav_gauss)
        self.blackmanwin_button.clicked.connect(self.apodize_wav_blackman)
        self.fft_button.clicked.connect(self.zerofill_fft)
        self.logscale_checkb.stateChanged.connect(self.zerofill_fft_finnished)

    def init_widgets(self):
        # files
        wav_label = QtWidgets.QLabel('Wav file')
        self.wav_path_line = QtWidgets.QLineEdit()
        self.wav_select = QtWidgets.QPushButton("Select")

        file_grid = QtWidgets.QGridLayout()
        file_grid.setSpacing(10)
        file_grid.addWidget(wav_label, 0, 0)
        file_grid.addWidget(self.wav_path_line, 0, 1)
        file_grid.addWidget(self.wav_select, 0, 2)

        # commands
        self.fid_button = QtWidgets.QPushButton("Reload")
        self.gausswin_button = QtWidgets.QPushButton("Gauss window")
        self.blackmanwin_button = QtWidgets.QPushButton("Blackman window")
        self.fft_button = QtWidgets.QPushButton("Zerofill + FFT")

        self.logscale_checkb = QtWidgets.QCheckBox("Log scale")
        self.logscale_checkb.setChecked(True)

        cmd_grid = QtWidgets.QGridLayout()
        cmd_grid.setSpacing(10)
        cmd_grid.addWidget(self.fid_button, 0, 0, 1, 3)
        cmd_grid.addWidget(self.gausswin_button, 1, 0)
        cmd_grid.addWidget(self.blackmanwin_button, 1, 1)
        cmd_grid.addWidget(self.fft_button, 1, 2)
        cmd_grid.addWidget(self.logscale_checkb, 2, 2)

        # canvas & text
        self.spec_canvas = canvas.KiteeSpecCanvas(self)
        self.spec_toolbar = NavigationToolbar(self.spec_canvas, self)
        self.main_text = QtWidgets.QTextEdit()
        self.main_text.setReadOnly(True)

        # all together
        main_vbox = QtWidgets.QVBoxLayout()
        main_vbox.addLayout(file_grid)
        main_vbox.addLayout(cmd_grid)
        main_vbox.addWidget(self.spec_canvas)
        main_vbox.addWidget(self.spec_toolbar)
        main_vbox.addWidget(self.main_text)

        self.setLayout(main_vbox)

    def init_main_text(self):
        self.main_text_contents = 'KiteeFFT_gui started ({:s})\n\n'.format(KITEEFFT_VERSION)
        self.update_main_text()

    def append_main_text(self, msg):
        self.main_text_contents += msg + '\n'
        self.update_main_text()
        self.main_text.verticalScrollBar().setValue(self.main_text.verticalScrollBar().maximum())

    def msg(self, msg):
        self.append_main_text(msg)

    def update_main_text(self):
        self.main_text.setText(self.main_text_contents)

    def clear_main_text(self):
        self.main_text_contents = ''
        self.update_main_text()

    def msg_status(self, msg):
        self.parent().statusBar().showMessage(msg)

    def msg_status_default(self):
        self.msg_status("Ready.")

    def show_select_wav(self):
        fname, filt = QtWidgets.QFileDialog.getOpenFileName(self, 'Open wav file', filter="Wav (*.wav)")
        self.wav_path_line.setText(fname)
        self.load_wav()

    def load_wav(self):
        self.clear_main_text()
        self.msg_status('Loading..')

        in_path = self.wav_path_line.text().strip()

        if in_path is not None and in_path != "":
            self.msg("reading {:s}".format(in_path))

            try:
                self.wspec.read(in_path)
                self.wspec.preproc_stereo()
                self.spec_canvas.update_figure_wav(self.wspec.time_x, self.wspec.wav_data)

                self.msg("read {:d} samples ({!s:s}) @ {:d}/sec".format(
                        self.wspec.wav_data.shape[0],
                        self.wspec.wav_data.dtype,
                        self.wspec.sample_rate
                    )
                )
            except ValueError as ve:
                self.msg(f'Error reading .wav: {ve}')

        self.msg_status_default()

    def apodize_wav_gauss(self):
        if not self.check_wav_data():
            return

        self.msg("gaussian apodization")
        self.wspec.preproc_window("gauss")
        self.spec_canvas.update_figure_wav(self.wspec.time_x, self.wspec.wav_data)

    def apodize_wav_blackman(self):
        if not self.check_wav_data():
            return

        self.msg("blackman apodization")
        self.wspec.preproc_window("blackman")
        self.spec_canvas.update_figure_wav(self.wspec.time_x, self.wspec.wav_data)

    def zerofill_fft(self):
        if not self.check_wav_data():
            return

        # run FFT in separate thread
        pthread = proc_thread.KiteeProcThread(parent=self)
        pthread.setup(self.wspec)
        pthread.msg_signal.connect(self.msg)
        pthread.finished.connect(self.zerofill_fft_finnished)
        pthread.start()

    def zerofill_fft_finnished(self):
        if self.wspec.freq_x is None or self.wspec.freq_y is None:
            return

        self.spec_canvas.update_figure_spec(
            self.wspec.freq_x,
            self.wspec.freq_y,
            log_scale=self.logscale_checkb.isChecked()
        )

    def check_wav_data(self):
        if self.wspec.time_x is None or self.wspec.wav_data is None:
            return False
        return True


class KiteeGUI(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        main_widg = KiteeMainWidget()
        self.setCentralWidget(main_widg)
        main_widg.msg_status_default()

        self.setGeometry(200, 200, 800, 700)
        self.setWindowTitle(KITEEFFT_VERSION)
