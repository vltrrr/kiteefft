import numpy as np

import matplotlib as mpl
from matplotlib import figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg

from PySide2 import QtWidgets, QtCore

from kiteefft.common import *

mpl.use('Qt5Agg')


class MPLCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=200, height=120, dpi=75):
        width = width / 25.4
        height = height / 25.4
        self.fig = figure.Figure(figsize=(width, height), dpi=dpi)

        self.axes = self.fig.add_subplot(111)
        self.compute_initial_figure()

        FigureCanvasQTAgg.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvasQTAgg.setSizePolicy(
            self,
            QtWidgets.QSizePolicy.Preferred,
            QtWidgets.QSizePolicy.Fixed
        )

        FigureCanvasQTAgg.updateGeometry(self)

    def compute_initial_figure(self):
        pass


class KiteeSpecCanvas(MPLCanvas):
    def __init__(self, *args, **kwargs):
        MPLCanvas.__init__(self, *args, **kwargs)

    def compute_initial_figure(self):
        self.setup_canvas()

    def setup_canvas(self):
        self.fig.patch.set_facecolor('white')
        self.fig.set_tight_layout(True)

    def update_figure_wav(self, time, wav_data):
        self.axes.cla()

        line, = self.axes.plot(time, wav_data)
        line.set_color('crimson')
        line.set_linewidth(1.5)

        x_max = np.amax(time)
        y_max = np.amax(np.abs(wav_data))
        self.axes.axis([0.0, x_max, -y_max, y_max])

        self.setup_canvas()
        self.draw()

    def update_figure_spec(self, freq, data_ft, log_scale=True):
        self.axes.cla()

        line, = self.axes.plot(freq, data_ft)
        line.set_color('crimson')
        line.set_linewidth(1.5)

        y_max = np.amax(data_ft)
        self.axes.axis((10, 20000, y_max * -0.05, y_max * 1.1))

        if log_scale:
            self.axes.set_xscale('log')

            # audio freq tics
            self.axes.set_xticks([20, 50, 100, 200, 500, 1000, 2000, 4000, 8000, 16000])
            self.axes.get_xaxis().set_major_formatter(mpl.ticker.ScalarFormatter())

        self.setup_canvas()
        self.draw()
