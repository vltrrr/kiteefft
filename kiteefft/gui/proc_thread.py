from PySide2 import QtCore

from kiteefft import wav_spectrum
from kiteefft.common import *


class KiteeProcThread(QtCore.QThread):
    msg_signal = QtCore.Signal(str)

    def setup(self, wspec: wav_spectrum.WavSpectrum):
        self.wspec = wspec

    def msg(self, msg):
        self.msg_signal.emit(msg)

    def run(self):
        self.msg("zerofill")
        self.wspec.preproc_zerofill()

        self.msg("FFT")
        self.wspec.fft()
        self.wspec.ft_normalize_n_of_samples()
