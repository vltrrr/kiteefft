from typing import List, Set, Dict, Tuple, Optional

import os
import warnings

import numpy as np
from scipy.io import wavfile
from scipy import signal


# _____________________________________________________________________________
# classes

class WavSpectrum():
    """
    Read, preprocess and FFT wav files
    """

    def __init__(self):
        self.wav_data: Optional[np.ndarray] = None
        self.wav_data_zfill: Optional[np.ndarray] = None
        self.time_x: Optional[np.ndarray] = None

        self.freq_x: Optional[np.ndarray] = None
        self.freq_y: Optional[np.ndarray] = None
        self.freq_y_db: Optional[np.ndarray] = None

        self.sample_rate: Optional[int] = None

    def read(self, in_path):
        warnings.filterwarnings('ignore', category=wavfile.WavFileWarning)

        self.sample_rate, self.wav_data = wavfile.read(in_path)

        samples = self.wav_data.shape[0]
        dwell_time = 1.0 / self.sample_rate
        total_time = dwell_time * (samples - 1)
        self.time_x = np.linspace(0, total_time, samples)

    def preproc_stereo(self):
        if self.wav_data.ndim == 2:
            self.wav_data = np.average(self.wav_data, axis=1)

    def preproc_lowpass(self, cutoff_hz=120.0, order=2):
        # freq as fraction of nyquist frequency
        cutoff = cutoff_hz / (0.5 * self.sample_rate)

        sos = signal.butter(order, cutoff, btype='lowpass', output='sos')
        self._apply_sos_filter(sos)

    def preproc_hipass(self, cutoff_hz=30.0, order=4):
        # freq as fraction of nyquist frequency
        cutoff = cutoff_hz / (0.5 * self.sample_rate)

        sos = signal.butter(order, cutoff, btype='highpass', output='sos')
        self._apply_sos_filter(sos)

    def preproc_bandpass(self, cutoff_lo=30.0, cutoff_hi=120.0, order=4):
        cutoff_lo = cutoff_lo / (0.5 * self.sample_rate)
        cutoff_hi = cutoff_hi / (0.5 * self.sample_rate)

        sos = signal.butter(order, (cutoff_lo, cutoff_hi), btype='bandpass', output='sos')
        self._apply_sos_filter(sos)

    def _apply_sos_filter(self, sos):
        self.wav_data = signal.sosfiltfilt(sos, self.wav_data)

    def preproc_window(self, window: str):
        assert window in ['gauss', 'blackman']
        assert self.wav_data is not None

        if window == 'gauss':
            size = self.wav_data.size
            stdev = size / 4
            gauss = signal.gaussian(size*2, stdev, sym=False)
            half_gaussian = gauss[size:]
            self.wav_data = self.wav_data * half_gaussian
        elif window == 'blackman':
            self.wav_data = self.wav_data * np.blackman(self.wav_data.size)

    def preproc_zerofill(self):
        new_size = 2 ** int.bit_length(self.wav_data.size)
        n_zeros = new_size - self.wav_data.size
        self.wav_data_zfill = np.hstack((self.wav_data, np.zeros(n_zeros)))

    def fft(self):
        samples = self.wav_data_zfill.size
        data_ft = np.fft.fft(self.wav_data_zfill)
        freq = np.fft.fftfreq(samples, d=1.0/self.sample_rate)

        half_pts = int(samples/2)
        data_ft = data_ft[0:half_pts]
        self.freq_x = freq[0:half_pts]

        self.freq_y = np.sqrt(data_ft.real ** 2 + data_ft.imag ** 2)

    def ft_normalize(self, norm_coef=None):
        if norm_coef is None:
            norm_coef = np.amax(self.freq_y)
        self.freq_y = self.freq_y / norm_coef

    def ft_normalize_n_of_samples(self):
        self.freq_y = self.freq_y / (self.freq_y.shape[0] * 0.01)

    def ft_calc_db(self, zero_db_ref):
        self.freq_y_db = 20 * np.log10(self.freq_y / zero_db_ref)
