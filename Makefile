venv:
	virtualenv --python=python3 venv

check-venv:
ifndef VIRTUAL_ENV
	$(error not inside virtualenvenv)
endif

install-pkg: check-venv
	pip install -e .

install-req: check-venv
	pip install -r requirements.txt

wheel: check-venv
	python ./setup.py bdist_wheel

freeze: check-venv
	pip freeze --exclude-editable > requirements.txt

pep8: check-venv
	pycodestyle kiteefft --max-line-length=120

mypy: check-venv
	mypy kiteefft --ignore-missing-imports

clean:
	rm -rf build
	rm -rf dist
	rm -rf kiteefft.egg-info

purge: clean
	rm -rf venv


.PHONY: check-venv install-pkg install-req wheel freeze clean purge
