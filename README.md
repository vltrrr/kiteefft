# KiteeFFT

KiteeFFT is a simple FFT analyzer/plotter for `.wav` files.

Most FFT visualizers use relatively small/short window to yield several spectra as a function of time.
KiteeFFT takes a spectroscopy approach instead (similar to NMR), and does FFT on the full data set.
The resulting spectrum lacks any temporal resolving power, but should yield accurate & sharp signals on any  frequencies present in the waveform.
This can reveal especially well constant hums and hisses, or just get really accurate frequency info compared to a basic FFT plot.

Made as an excersise in PyQT5 with embedded matplotlib canvas/figure.


## Screenshots

![kitee_sinebass_riedlxl.png](img/kitee_olohuone.png)


## Installation

Requirements:

* Python3
* NumPy
* SciPy
* Matplotlib
* PySide2

In Debian based distros, you can start with:

```
apt-get install python3 python3-pyside2 python3-virtualenv python3-pip
```

And let pip take care of the rest in a dedicated virtualenv.
Clone or download the repo:

```
git clone https://gitlab.com/vltrrr/kiteefft.git
```

install virtualenv and the package:

```
cd kiteefft

# create virtualenv
make venv
. venv/bin/activate

# install in editable mode
make install-pkg

# OR build wheel & install
make wheel
pip install dist/kiteefft-0.4.0-py3-none-any.whl

```

Ready to launch:

```
kiteefft

```


# License #

BSD 3-Clause. See LICENSE in this repository.
