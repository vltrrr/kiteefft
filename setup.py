from setuptools import setup, find_packages
from codecs import open

with open('VERSION', 'r', 'utf-8') as handle:
	VERSION = handle.read()


setup(
	name='kiteefft',
	version=VERSION,

	description='KiteeFFT .wav FFT plotter',
	long_description='',
	url='https://vltr.fi',
	author='Valtteri Mäkelä',
	author_email='valtteri.makela@iki.fi',
	license='BSD',

	classifiers=[
		'Development Status :: 4 - Beta',
		'License :: OSI Approved :: BSD License',

		'Topic :: Scientific/Engineering',
		'Topic :: Scientific/Engineering :: Visualization',

		'Programming Language :: Python :: 3',
		'Programming Language :: Python :: 3.6',
		'Programming Language :: Python :: 3.7',
		'Programming Language :: Python :: 3.8',
	],

	packages=find_packages(exclude=['kiteefft_test']),

	install_requires=[
		'numpy>=1.12.0',
		'scipy>=0.18.0',
		'matplotlib>=2.0.0',
		'pyside2>=5.15.0',
	],

	entry_points={
		'console_scripts': [
			'kiteefft_cli=kiteefft.main_cli:main',
		],
		'gui_scripts': [
			'kiteefft=kiteefft.main_gui:main',
		],
	},
)
